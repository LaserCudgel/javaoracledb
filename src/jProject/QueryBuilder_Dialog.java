package jProject;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ScrollPaneConstants;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class QueryBuilder_Dialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8894253115678620643L;
	private final JPanel contentPanel = new JPanel();
	Statement st = ConnectingToDB_Dialog.con.stmt;
	JList<String> listOfTables = new JList<String>();
	JList<String> listOfColumns = new JList<String>();
	JScrollPane scrollPane = new JScrollPane();

	/**
	 * Create the dialog.
	 */
	public QueryBuilder_Dialog() {
		setTitle("\u041A\u043E\u043D\u0441\u0442\u0440\u0443\u043A\u0442\u043E\u0440 \u0437\u0430\u043F\u0440\u043E\u0441\u043E\u0432");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 490, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.add(new JScrollBar());
		/* ListOfColumns OPEN */

		/* ListOfDB OPEN */
		String query = "SELECT table_name FROM user_tables";
		DefaultListModel<String> DLM_DB = new DefaultListModel<String>();
		ResultSet rs = null;
		try {
			rs = st.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String table_name = rs.getString("table_name");
				DLM_DB.addElement(table_name);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, e1.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		/* ListOfDB CLOSE*/

		JLabel label = new JLabel(
				"\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0442\u0430\u0431\u043B\u0438\u0446\u044B");
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		JLabel label_1 = new JLabel(
				"\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0441\u0442\u043E\u043B\u0431\u0446\u044B");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		scrollPane.setViewportView(listOfColumns);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportView(listOfTables);
		
				listOfTables.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						/* listOfColumns OPEN */
						String query = "SELECT column_name FROM user_tab_columns WHERE table_name IN ("
								+ getSelectedTables() + ")";
						
						DefaultListModel<String> DLM_COL = new DefaultListModel<String>();
						ResultSet rs = null;
						try {
							rs = st.executeQuery(query);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						try {
							while (rs.next()) {
								String column_name = rs.getString("column_name");
								DLM_COL.addElement(column_name);
							}
						} catch (SQLException e) {
							JOptionPane.showMessageDialog(null,
									e.getLocalizedMessage(), "������",
									JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
						try {
							rs.close();
						} catch (SQLException e) {
							JOptionPane.showMessageDialog(null,
									e.getLocalizedMessage(), "������",
									JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
						listOfColumns.setModel(DLM_COL);
						/* listOfColumns CLOSE*/
					}
				});
				listOfTables.setModel(DLM_DB);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label)
						.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 211, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(label_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 356, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 356, GroupLayout.PREFERRED_SIZE))
					.addGap(30))
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					QueryView queryView = null;
					public void actionPerformed(ActionEvent e) {
						String query = getFinalQuery(getSelectedTables(), getSelectedCells());
						System.out.println(query);
						queryView = new QueryView(query);
						queryView.setVisible(true);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	protected String getSelectedTables() {
		String selectedValues_str = "";
		List<String> selectedValues = listOfTables.getSelectedValuesList();
		for (Iterator<String> i = selectedValues.iterator(); i
				.hasNext();) {
			selectedValues_str += i.next().toString();
			if (i.hasNext()) {
				selectedValues_str += "','";
			}
		}
		selectedValues_str = "'" + selectedValues_str;
		selectedValues_str.replaceAll("[|]", "','");
		selectedValues_str.replaceAll("\'+", "'");
//		System.out.println(query);
		
		return selectedValues_str+"'";
	}

	protected String getSelectedCells() {
		String selectedValues_str = "";
		List<String> selectedValues = listOfColumns.getSelectedValuesList();
		for (Iterator<String> i = selectedValues.iterator(); i
				.hasNext();) {
			selectedValues_str += i.next().toString();
			if (i.hasNext()) {
				selectedValues_str += "','";
			}
		}
		selectedValues_str = "'" + selectedValues_str;
		selectedValues_str.replaceAll("[|]", "','");
		selectedValues_str.replaceAll("\'+", "'");
//		System.out.println(selectedValues_str);
		
		return selectedValues_str+"'";
	}

	protected String getFinalQuery(String aTables, String aCells) {
		aTables = aTables.replaceAll("'", "");
		aCells = aCells.replaceAll("'", "");
		String query = "SELECT "+aCells+" FROM "+aTables;
		System.out.println(query);
		return query;
	}
}
