package jProject;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.sql.SQLException;
import java.sql.Statement;

import java.awt.Font;

import javax.swing.JEditorPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SQLRequests extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7808268491984215249L;

	/**
	 * Create the frame.
	 */
	public SQLRequests() {
		setTitle("\u0412\u044B\u043E\u043B\u043D\u0438\u0442\u044C \u0437\u0430\u043F\u0440\u043E\u0441");
		setBounds(100, 100, 449, 359);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		final JEditorPane dtrpnCreateTablePersons = new JEditorPane();
		dtrpnCreateTablePersons.setText("CREATE TABLE Persons\r\n(\r\nPersonID int,\r\nLastName varchar(255),\r\nFirstName varchar(255),\r\nAddress varchar(255),\r\nCity varchar(255)\r\n)");
		dtrpnCreateTablePersons.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JButton button = new JButton("\u0412\u044B\u043F\u043E\u043B\u043D\u0438\u0442\u044C");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String query = dtrpnCreateTablePersons.getText();
				Statement st = ConnectingToDB_Dialog.con.stmt;
				query.replaceAll("\n|\r\n", " ");
				if (query.toUpperCase().contains("SELECT")) {
					QueryView queryView = null;
					String queryToView = query;
					System.out.println(queryToView);
					queryView = new QueryView(queryToView);
					queryView.setVisible(true);
				}
				try {
					st.executeQuery(query);
					System.out.println("Request executed successfully.");
				} catch (SQLException e) {
					System.out.println("Request failed.");
					JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
							"������", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(dtrpnCreateTablePersons, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
						.addComponent(button))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(dtrpnCreateTablePersons, GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(button)
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);

	}
}
