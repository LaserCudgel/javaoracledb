package jProject;

import java.awt.Font;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JList;
import javax.swing.JButton;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TableEdit extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1346321727932295330L;
	private JPanel contentPane;
	private JTable table;
	private JList<String> list = new JList<String>();
	private JTextField textField;
	private JLabel label;
	private ResultSet rs;
	final Statement st = ConnectingToDB_Dialog.con.stmt;
	private JButton button;

	/**
	 * Create the frame.
	 */
	public TableEdit() {
		setTitle("\u0420\u0435\u0434\u043E\u043A\u0442\u043E\u0440 \u0437\u0430\u043F\u0438\u0441\u0435\u0439");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 666, 461);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		table = new JTable();
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				switch (arg0.getKeyChar()) {
				// �������� ������
				case KeyEvent.VK_DELETE:
					deleteRow();
					rePaintTable();
					break;

				// �������������� ��������
				case KeyEvent.VK_ENTER:
					editCell();
					rePaintTable();
					break;
				default:
					break;
				}
			}
		});
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Class<?> typeTable�ell = null;
				int selectedTableCell = table.getSelectedColumn();
				typeTable�ell = table.getColumnClass(selectedTableCell);
				System.out.println("Selected: Column � "
						+ table.getSelectedColumn() + "; Row � "
						+ table.getSelectedRow() + "; Type � "
						+ typeTable�ell.getSimpleName());
				// textField.setText(typeTable�ell.getSimpleName());
			}
		});

		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.setName("selected_table");

		String query = "SELECT table_name FROM user_tables";
		DefaultListModel<String> DLM = new DefaultListModel<String>();
		try {
			rs = st.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String table_name = rs.getString("table_name");
				DLM.addElement(table_name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// list = new JList();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				rePaintTable();

				/* TEXTFIELD OPEN */
				Class<?> typeTable�ell = null;
				String finalText = "";
				for (int i = 0; i < table.getColumnCount(); i++) {
					typeTable�ell = table.getColumnClass(i);
					finalText += typeTable�ell.getSimpleName();
					if (i != table.getColumnCount() - 1) {
						finalText += ", ";
					}
				}
				textField.setText(finalText);
				/* TEXTFIELD CLOSE */
			}
		});

		list.setFont(new Font("Tahoma", Font.PLAIN, 15));
		list.setModel(DLM);
		list.setName("list_of_tables");

		JButton btnNewButton = new JButton(
				"\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String txtField_text = "'" + textField.getText() + "'";
				txtField_text = txtField_text.replaceAll("\\s+", "");
				txtField_text = txtField_text.replaceAll(",", "','");
				if (txtField_text == "") {
					JOptionPane.showMessageDialog(null, "������",
							"������ �������� �����", JOptionPane.ERROR_MESSAGE);
					return;
				}
				// INSERT INTO <�������� �������> SELECT <��� �������>
				String query = "INSERT INTO "
						+ list.getSelectedValue().toString() + " VALUES ("
						+ txtField_text + ")";
				System.out.println(query);
				try {
					st.executeQuery(query);
					System.out.println("Request executed successfully.");
				} catch (SQLException e) {
					System.out.println("Request failed.");
					JOptionPane.showMessageDialog(null,
							e.getLocalizedMessage(), "������",
							JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
				rePaintTable();
			}
		});

		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.setColumns(10);

		label = new JLabel(
				"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043D\u0430\u0437\u0432\u0430\u043D\u0438\u044F \u043F\u043E\u043B\u0435\u0439 \u0447\u0435\u0440\u0435\u0437 \u0437\u0430\u043F\u044F\u0442\u0443\u044E:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));

		button = new JButton(
				"\u0421\u043E\u0441\u0442\u0430\u0432\u043D\u044B\u0435 \u043F\u0435\u0440\u0432\u0438\u0447\u043D\u044B\u0435 \u043A\u043B\u044E\u0447\u0438");
		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (list.isSelectionEmpty()) {
					JOptionPane.showMessageDialog(null,
							"���������� ������� �������", "������",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				System.out
						.println("��������� ��������� ��������� ���� ��� �������: "
								+ list.getSelectedValue());
				jProject.DefineCompoundKey DefineCompoundKey = new DefineCompoundKey(
						list.getSelectedValue());
				DefineCompoundKey.setVisible(true);
			}
		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 15));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(list, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(table, GroupLayout.PREFERRED_SIZE, 511, GroupLayout.PREFERRED_SIZE))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE))
						.addComponent(button, Alignment.TRAILING)
						.addComponent(label))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(list, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(table, GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(button)
					.addGap(54))
		);
		contentPane.setLayout(gl_contentPane);
	}

	protected void editCell() {
		int row = 0;
		System.out.println(table.getRowCount());
		System.out.println(table.getSelectedRow());
		if (table.getSelectedRow() == 0) {
			row = table.getRowCount() - 1;
		} else {
			row = table.getSelectedRow() - 1;
		}
		int column = table.getSelectedColumn();
		String columnName = table.getColumnName(column);
		String tableName = list.getSelectedValue().toString();
		// TODO: PK ��������, ����� ����� �������� �� ���������, ������ �
		// ��������
		Object PK = table.getValueAt(row, 0);
		Object cellData = table.getValueAt(row, column);
		// UPDATE table_name SET column1=value1,column2=value2,... WHERE
		// some_column=some_value;
		String query = "UPDATE " + tableName + " SET " + columnName + "='"
				+ cellData + "' WHERE " + table.getColumnName(0) + "='" + PK
				+ "'";
		System.out.println(query);
		try {
			st.executeQuery(query);
			System.out.println("Request executed successfully.");
		} catch (SQLException e) {
			System.out.println("Request failed.");
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	protected void deleteRow() {
		int row = table.getSelectedRow();
		int column = 0;
		String columnName = table.getColumnName(column);
		String tableName = list.getSelectedValue().toString();
		Object cellData = table.getValueAt(row, column);
		// DELETE FROM table_name WHERE some_column=some_value;
		String query = "DELETE FROM " + tableName + " WHERE " + columnName
				+ "=" + cellData;
		System.out.println(query);
		try {
			st.executeQuery(query);
			System.out.println("Request executed successfully.");
		} catch (SQLException e) {
			System.out.println("Request failed.");
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	/**
	 * �������������� ��������� � JList �������
	 */
	protected void rePaintTable() {
		DatabaseTableModel DTM = new DatabaseTableModel();
		String query = "SELECT " + list.getSelectedValue() + ".* FROM "
				+ list.getSelectedValue();
		Statement st = ConnectingToDB_Dialog.con.stmt;

		ResultSet rs = null;
		try {
			rs = st.executeQuery(query);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			DTM.setDataSource(rs);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		table.setModel(DTM);
	}
}
