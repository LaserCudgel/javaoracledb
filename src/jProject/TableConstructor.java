package jProject;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.NumberFormatter;

import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.JComboBox;

import java.lang.reflect.Field;

import javax.swing.JScrollPane;
import javax.swing.JCheckBox;

public class TableConstructor extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3240288205417309157L;
	private final JPanel contentPanel = new JPanel();
	private JTextField nameOfColumn_txtField;
	private JComboBox<String> typeOfColumn_comboBox;
	private JTable table;
	JCheckBox isPKcheckBox;
	JCheckBox isNOTNULLcheckBox = new JCheckBox("NOT NULL");

	DefaultTableModel DfTM = new DefaultTableModel();
	DefaultComboBoxModel<String> CmbMD = new DefaultComboBoxModel<String>();

	private JTextField sizeOfColumn_txtField;
	List<String> columns = new ArrayList<String>();

	String resizebleTypes = "CHAR VARCHAR TINYINT SMALLINT MEDIUMINT INT BIGINT FLOAT DOUBLE DECIMAL";
	String tableName_copy;

	public boolean tableIsExist;

	/**
	 * Create the dialog.
	 */
	public TableConstructor(String tableName) {
		tableIsExist = false;
		tableName_copy = tableName.toUpperCase();
		setTitle("�������� ������� � ������� \"" + tableName_copy + "\"");
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 666, 323);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		/*
		 * http://stackoverflow.com/questions/6437790/jdbc-get-the-sql-type-name-
		 * from-java-sql-type-code
		 */
		Map<Integer, String> jdbcMappings = getAllJdbcTypeNames();
		/*
		 * jdbcMappings: {0=NULL, 1=CHAR, 70=DATALINK, 2=NUMERIC, 3=DECIMAL,
		 * 4=INTEGER, 5=SMALLINT, 6=FLOAT, 7=REAL, 8=DOUBLE, 12=VARCHAR,
		 * -16=LONGNVARCHAR, -15=NCHAR, 16=BOOLEAN, -9=NVARCHAR, -8=ROWID,
		 * 93=TIMESTAMP, -7=BIT, 92=TIME, -6=TINYINT, 1111=OTHER, -5=BIGINT,
		 * -4=LONGVARBINARY, -3=VARBINARY, 91=DATE, -2=BINARY, -1=LONGVARCHAR,
		 * 2002=STRUCT, 2003=ARRAY, 2000=JAVA_OBJECT, 2001=DISTINCT, 2006=REF,
		 * 2004=BLOB, 2005=CLOB, 2011=NCLOB, 2009=SQLXML}
		 */
		// sqlTypes.
		Collection<String> elements = jdbcMappings.values();
		String[] str = elements.toString().replaceAll("\\s+|\\[|\\]", "")
				.split(",");

		// ���������� ������ ������ ComboBox'� ������ ������ sql
		for (String s : str) {
			CmbMD.addElement(s);
		}

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(197, 31, 452, 87);
		scrollPane.setBorder(null);
		scrollPane.setFont(new Font("Tahoma", Font.PLAIN, 15));
		scrollPane.setAlignmentY(Component.BOTTOM_ALIGNMENT);

		JPanel panel = new JPanel();
		panel.setBounds(17, 153, 627, 95);

		JLabel label = new JLabel("\u041F\u043E\u043B\u044F:");
		label.setBounds(17, 31, 39, 19);
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel lblNewLabel = new JLabel(
				"\u0422\u0438\u043F \u0434\u0430\u043D\u043D\u044B\u0445");
		lblNewLabel.setBounds(17, 47, 167, 19);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel label_2 = new JLabel(
				"\u0420\u0430\u0437\u043C\u0435\u0440\u043D\u043E\u0441\u0442\u044C");
		label_2.setBounds(17, 63, 167, 19);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel label_3 = new JLabel(
				"\u042D\u0442\u043E \u043F\u0435\u0440\u0432\u0438\u0447\u043D\u044B\u0439 \u043A\u043B\u044E\u0447");
		label_3.setBounds(17, 95, 167, 19);
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel label_4 = new JLabel(
				"\u041D\u0435 \u043C\u043E\u0436\u0435\u0442 \u0431\u044B\u0442\u044C \u043F\u0443\u0441\u0442\u044B\u043C");
		label_4.setBounds(17, 79, 167, 19);
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JButton addColumn_bttn = new JButton(
				"\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C");
		addColumn_bttn.addActionListener(new ActionListener() {
			// ���������� ������� � �������
			public void actionPerformed(ActionEvent e) {
				String nameOfcolumnName = nameOfColumn_txtField.getText()
						.trim();
				String nameOfcolumnType = typeOfColumn_comboBox
						.getSelectedItem().toString();
				String nameOfcolumnSize = sizeOfColumn_txtField.getText()
						.trim();
				if (!sizeOfColumn_txtField.isEnabled()) {
					nameOfcolumnSize = null;
				}
				boolean isPK = isPKcheckBox.isSelected();
				boolean isNOTNULL = isNOTNULLcheckBox.isSelected();
				if (nameOfcolumnName.isEmpty()) {
					System.out
							.println("�������� ������� �� ����� ���� ������!");
					JOptionPane.showMessageDialog(null,
							"�������� ������� �� ����� ���� ������!", "������",
							JOptionPane.ERROR_MESSAGE);
					return;
				}

				String nameIsPK = null;
				String nameIsNOTNULL = null;
				if (isNOTNULL) {
					nameIsNOTNULL = "NOT NULL";
				} else {
					nameIsNOTNULL = "";
				}
				Object[] column = { nameOfcolumnType, nameOfcolumnSize,
						isNOTNULL, isPK };
				if (DfTM.findColumn(nameOfcolumnName) == -1) {
					DfTM.addColumn(nameOfcolumnName, column);
					if (isPK) {
						nameIsPK = "PRIMARY KEY ";
						System.out.println("� ������� �������� ������� \""
								+ nameOfcolumnName + "\" ���� "
								+ nameOfcolumnType + " " + nameIsNOTNULL
								+ ", ������������ " + nameOfcolumnSize
								+ ". ���� ������� �������� ��������� ������.");
					} else {
						nameIsPK = "";
						System.out.println("� ������� �������� ������� \""
								+ nameOfcolumnName + "\" ���� "
								+ nameOfcolumnType + " " + nameIsNOTNULL
								+ ", ������������ " + nameOfcolumnSize);
					}
				} else {
					System.out.println("�������  \"" + nameOfcolumnName
							+ "\" ��� ���������� � �������!");
					JOptionPane.showMessageDialog(null,
							"������� � ����� ������ ��� ����������!", "������",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				if (sizeOfColumn_txtField.isEnabled()) {
					columns.add((nameOfcolumnName + " " + nameOfcolumnType
							+ " (" + nameOfcolumnSize + ") " + nameIsNOTNULL
							+ " " + nameIsPK).trim());
				} else {
					columns.add((nameOfcolumnName + " " + nameOfcolumnType
							+ " " + nameIsNOTNULL + " " + nameIsPK).trim());
				}

				for (String col : columns) {
					System.out.println("Column: " + col);
				}
				table.setModel(DfTM);
				isPKcheckBox.setSelected(false);
			}
		});
		addColumn_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));

		nameOfColumn_txtField = new JTextField();
		nameOfColumn_txtField
				.setToolTipText("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043D\u0430\u0437\u0432\u0430\u043D\u0438\u0435 \u0441\u0442\u043E\u043B\u0431\u0446\u0430");
		nameOfColumn_txtField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		nameOfColumn_txtField.setColumns(10);
		JLabel label_1 = new JLabel(
				"\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C \u0441\u0442\u043E\u043B\u0431\u0435\u0446");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 15));

		sizeOfColumn_txtField = new JFormattedTextField(new NumberFormatter());
		sizeOfColumn_txtField
				.setToolTipText("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0440\u0430\u0437\u043C\u0435\u0440");
		sizeOfColumn_txtField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		sizeOfColumn_txtField.setText("16");
		sizeOfColumn_txtField.setColumns(10);

		typeOfColumn_comboBox = new JComboBox<String>(CmbMD);
		typeOfColumn_comboBox.setSelectedIndex(1);
		typeOfColumn_comboBox
				.setToolTipText("\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0442\u0438\u043F \u0434\u0430\u043D\u043D\u044B\u0445, \u0445\u0440\u0430\u043D\u044F\u0449\u0438\u0445\u0441\u044F \u0432 \u044D\u0442\u043E\u043C \u0441\u0442\u043E\u043B\u0431\u0446\u0435");
		typeOfColumn_comboBox.setFont(new Font("Tahoma", Font.PLAIN, 15));
		typeOfColumn_comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean isResizebleType = resizebleTypes
						.contains(typeOfColumn_comboBox.getSelectedItem()
								.toString().toUpperCase());
				sizeOfColumn_txtField.setEnabled(isResizebleType);
				System.out.println("����������� ���� ���������: "
						+ isResizebleType);
			}
		});

		isPKcheckBox = new JCheckBox(
				"\u041F\u0435\u0440\u0432\u0438\u0447\u043D\u044B\u0439 \u043A\u043B\u044E\u0447");
		isPKcheckBox.setFont(new Font("Tahoma", Font.PLAIN, 15));

		isNOTNULLcheckBox.setFont(new Font("Tahoma", Font.PLAIN, 15));
		isNOTNULLcheckBox.setSelected(true);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.LEADING)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addComponent(
																		nameOfColumn_txtField,
																		GroupLayout.PREFERRED_SIZE,
																		158,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		typeOfColumn_comboBox,
																		GroupLayout.PREFERRED_SIZE,
																		119,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		sizeOfColumn_txtField,
																		GroupLayout.PREFERRED_SIZE,
																		68,
																		GroupLayout.PREFERRED_SIZE))
												.addComponent(
														label_1,
														GroupLayout.PREFERRED_SIZE,
														189,
														GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.LEADING)
												.addComponent(isNOTNULLcheckBox)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addComponent(
																		isPKcheckBox)
																.addPreferredGap(
																		ComponentPlacement.RELATED)
																.addComponent(
																		addColumn_bttn,
																		GroupLayout.PREFERRED_SIZE,
																		112,
																		GroupLayout.PREFERRED_SIZE)))
								.addContainerGap(GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addComponent(label_1)
								.addGap(5)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.BASELINE)
												.addComponent(
														nameOfColumn_txtField,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														typeOfColumn_comboBox,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														sizeOfColumn_txtField,
														GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(isPKcheckBox)
												.addComponent(addColumn_bttn))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(isNOTNULLcheckBox)
								.addContainerGap(10, Short.MAX_VALUE)));
		panel.setLayout(gl_panel);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] {}));
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		scrollPane.setViewportView(table);
		contentPanel.setLayout(null);
		contentPanel.add(label_4);
		contentPanel.add(label_3);
		contentPanel.add(label_2);
		contentPanel.add(lblNewLabel);
		contentPanel.add(scrollPane);
		contentPanel.add(panel);
		contentPanel.add(label);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						// ������ �������� ����� � �� �������� � �������
						// ���������� ������� � ��
						addTableToDB(tableName_copy, columns);
						DBEdit.rePullingListDB();
						setVisible(false);
					}
				});
				okButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		if (tableIsExist(tableName_copy)) {
			int reply = JOptionPane.showConfirmDialog(null, "������� "
					+ tableName_copy
					+ " ��� ����������, ������ ������ � �� ���������? ", null,
					JOptionPane.YES_NO_OPTION);
			if (reply == JOptionPane.YES_OPTION) {
				addingExistColumnsToJTable(tableName_copy);
				tableIsExist = true;
				// throw new
				// NullPointerException("�������� ��� ������� ��� ������������!");
			} else {
				throw new NullPointerException(
						"�������������� ������� ��������.");
			}
		}
	}

	private void addingExistColumnsToJTable(String aTabelName) {
		DatabaseMetaData metaData = ConnectingToDB_Dialog.con.getMetaData();
		ResultSet tablesResultSet = null;
		ResultSet tablesPKs = null;

		Map<Integer, String> iJdbcMappings = getAllJdbcTypeNames();
		String colNameOfPrimaryKey = "";
		Object[] column = new Object[4];

		try {
			tablesResultSet = metaData
					.getColumns(null,
							ConnectingToDB_Dialog.login.toUpperCase(),
							aTabelName, null);
			tablesPKs = metaData.getPrimaryKeys(null,
					ConnectingToDB_Dialog.login.toUpperCase(), aTabelName);
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, e1.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		try {
			while (tablesPKs.next()) {
				colNameOfPrimaryKey +=tablesPKs.getString("COLUMN_NAME") + " ";
			}
			System.out
					.println("� ������� ���������� ��������� ��������� �����: "
							+ colNameOfPrimaryKey);
			while (tablesResultSet.next()) {
				String colName = tablesResultSet.getString("COLUMN_NAME");
				String colType = tablesResultSet.getString("DATA_TYPE");
				String colSize = tablesResultSet.getString("COLUMN_SIZE");
				String colNull = tablesResultSet.getString("NULLABLE");
				boolean nullable = false;
				if (colNull.equals("0")) {
					nullable = true;
				}
				String dataType = iJdbcMappings.get(Integer.parseInt(colType));
				if (tablesResultSet.getString("TABLE_NAME").equals(aTabelName)) {
					System.out.println("" + "COLUMN_NAME: " + colName
							+ "; DATA_TYPE: " + dataType + "; COLUMN_SIZE: "
							+ colSize + "; NULLABLE: " + nullable);
					column[0] = dataType;	// ���������� ������ "��� ������" 
					column[1] = colSize;	// ���������� ������ "�����������"
					column[2] = nullable;	// ���������� ������ "NOT NULL"
					column[3] = "false";	// ���������� ������ "��������� ����"
					if (colNameOfPrimaryKey.indexOf(colName) != -1) {
						column[3] = "true";
					}
					DfTM.addColumn(colName, column);
				}
			}
			table.setModel(DfTM);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	protected void addColumnToTable(String aTabelName, List<String> aColumns) {
		/*
		 * String[] allColNames = new String[aColumns.size()]; String[]
		 * allColTypes = new String[aColumns.size()];
		 */

		String colNamesForQuery = new String();
		for (Object string : aColumns) {
			if (!colNamesForQuery.isEmpty())
				colNamesForQuery += ",";
			colNamesForQuery += string.toString();
		}

		/*
		 * int i = 0; for (String string : aColumns) { allColNames[i] =
		 * string.split(" ")[0]; allColTypes[i] = string.split(" ")[1];
		 * 
		 * System.out.println("�������� ������� '" + allColNames[i] +
		 * "', ���� '" + allColTypes[i] + "'");
		 * 
		 * i++; }
		 */
		String query = "ALTER TABLE " + aTabelName + " ADD " + " ("
				+ colNamesForQuery + ")";
		System.out.println(query);
		executeQuery(query);
	}

	/*
	 * private boolean columnIsExist(String aTabelName, String aColName) {
	 * DatabaseMetaData metaData = ConnectingToDB_Dialog.con.getMetaData();
	 * ResultSet tablesResultSet = null; boolean result = false; try {
	 * tablesResultSet = metaData .getColumns(null,
	 * ConnectingToDB_Dialog.login.toUpperCase(), aTabelName, null); } catch
	 * (SQLException e1) { JOptionPane.showMessageDialog(null,
	 * e1.getLocalizedMessage(), "������", JOptionPane.ERROR_MESSAGE);
	 * e1.printStackTrace(); } try { while (tablesResultSet.next()) { if
	 * (tablesResultSet.getString("COLUMN_NAME").equals(aColName)) { result =
	 * true; } } } catch (SQLException e) { JOptionPane.showMessageDialog(null,
	 * e.getLocalizedMessage(), "������", JOptionPane.ERROR_MESSAGE);
	 * e.printStackTrace(); } return result; }
	 */

	protected void createTable(String aTabelName, List<String> aColumns) {
		String colNamesForQuery = new String();
		for (Object string : aColumns) {
			if (!colNamesForQuery.isEmpty())
				colNamesForQuery += ",";
			colNamesForQuery += string.toString();
		}
		// System.out.println("colNamesForQuery: " + colNamesForQuery);
		String query = "CREATE TABLE " + aTabelName + " (" + colNamesForQuery
				+ ")";
		System.out.println(query);
		executeQuery(query);
	}

	private boolean tableIsExist(String aTabelName) {
		DatabaseMetaData metaData = ConnectingToDB_Dialog.con.getMetaData();
		ResultSet tablesResultSet = null;
		boolean result = false;
		try {
			tablesResultSet = metaData.getTables(null,
					ConnectingToDB_Dialog.login.toUpperCase(), aTabelName,
					new String[] { "TABLE" });
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, e1.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		try {
			while (tablesResultSet.next()) {
				if (tablesResultSet.getString("TABLE_NAME").equals(aTabelName)) {
					System.out.println("������� " + aTabelName
							+ " ��� ����������!");
					result = true;
				}
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * ������ ������� � �� ��� ��������� � ��� ������������ � �� ������� �����
	 * ����
	 * 
	 * @param aTabelName
	 *            ��� �������
	 * @param aColumns
	 *            ������, ���������� �������� �������
	 */
	protected void addTableToDB(String aTabelName, List<String> aColumns) {
		if (tableIsExist(aTabelName)) {
			addColumnToTable(aTabelName, aColumns);
			return;
		} else {
			createTable(aTabelName, aColumns);
		}
	}

	private void executeQuery(String aQuery) {
		System.out.println(aQuery);
		Statement st = ConnectingToDB_Dialog.con.stmt;

		try {
			st.executeQuery(aQuery);
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, e1.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
	}

	// ��������� ��� �������� ��� ����� ������, �������������� ��������� jdbc
	public Map<Integer, String> getAllJdbcTypeNames() {
		Map<Integer, String> result = new HashMap<Integer, String>();
		for (Field field : Types.class.getFields()) {
			try {
				result.put((Integer) field.get(null), field.getName());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
