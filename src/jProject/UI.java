package jProject;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;

import javax.swing.JSeparator;

import java.awt.event.MouseAdapter;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SwingConstants;

public class UI {

	public JFrame frame;
	static JLabel statusLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI window = new UI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JSeparator separator = new JSeparator();
		separator.setBounds(12, 298, 408, 2);
		frame.getContentPane().setLayout(null);

		statusLabel = new JLabel(
				"\u0412\u044B \u043D\u0435 \u0432\u043E\u0448\u043B\u0438");
		statusLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		statusLabel.setBounds(22, 318, 273, 16);
		frame.getContentPane().add(statusLabel);
		frame.getContentPane().add(separator);

		JButton autority_bttn = new JButton(
				"\u0410\u0432\u0442\u043E\u0440\u0438\u0437\u0430\u0446\u0438\u044F");
		autority_bttn.setHorizontalAlignment(SwingConstants.LEFT);
		autority_bttn.addActionListener(new ActionListener() {
			JDialog ConnectingDialog;

			public void actionPerformed(ActionEvent e) {
				if (ConnectingDialog == null) {
					ConnectingDialog = new ConnectingToDB_Dialog();
				} else {
					ConnectingDialog.setVisible(true);
				}
			}
		});
		autority_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		autority_bttn.setBounds(66, 58, 296, 27);
		frame.getContentPane().add(autority_bttn);

		JButton editingTable_bttn = new JButton(
				"\u0420\u0435\u0434\u0430\u043A\u0442\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u0435 \u0437\u0430\u043F\u0438\u0441\u0435\u0439 \u0432 \u0442\u0430\u0431\u043B\u0438\u0446\u0435");
		editingTable_bttn.setHorizontalAlignment(SwingConstants.LEFT);
		editingTable_bttn.addActionListener(new ActionListener() {
			jProject.TableEdit TableEdit;

			public void actionPerformed(ActionEvent e) {
				if (!isAutorityInDB()) {
					return;
				}
				if (TableEdit == null) {
					TableEdit = new TableEdit();
				} else {
					TableEdit.setVisible(true);
				}
			}
		});
		editingTable_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));

		editingTable_bttn.setBounds(66, 98, 296, 27);
		frame.getContentPane().add(editingTable_bttn);

		JButton editingDB_bttn = new JButton(
				"\u0420\u0435\u0434\u0430\u043A\u0442\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u0435 \u0442\u0430\u0431\u043B\u0438\u0446");
		editingDB_bttn.setHorizontalAlignment(SwingConstants.LEFT);
		editingDB_bttn.addActionListener(new ActionListener() {
			jProject.DBEdit DBEdit;

			public void actionPerformed(ActionEvent e) {
				if (!isAutorityInDB()) {
					return;
				}
				if (DBEdit == null) {
					DBEdit = new DBEdit();
				} else {
					DBEdit.setVisible(true);
				}
			}
		});
		editingDB_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		editingDB_bttn.addMouseListener(new MouseAdapter() {

		});
		editingDB_bttn.setBounds(66, 138, 296, 27);
		frame.getContentPane().add(editingDB_bttn);

		JButton query_bttn = new JButton(
				"\u0417\u0430\u043F\u0440\u043E\u0441\u044B");
		query_bttn.setHorizontalAlignment(SwingConstants.LEFT);
		query_bttn.addActionListener(new ActionListener() {
			jProject.SQLRequests SQLRequests;

			public void actionPerformed(ActionEvent e) {
				if (!isAutorityInDB()) {
					return;
				}
				if (SQLRequests == null) {
					SQLRequests = new SQLRequests();
				} else {
					SQLRequests.setVisible(true);
				}
			}
		});
		query_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));

		query_bttn.setBounds(66, 178, 296, 27);
		frame.getContentPane().add(query_bttn);

		JButton queryBuilder_bttn = new JButton("\u041E\u0442\u0447\u0451\u0442\u044B");
		queryBuilder_bttn.setHorizontalAlignment(SwingConstants.LEFT);
		queryBuilder_bttn.addActionListener(new ActionListener() {
			jProject.QueryBuilder_Dialog QueryBuilder_Dialog;

			public void actionPerformed(ActionEvent e) {
				if (!isAutorityInDB()) {
					return;
				}
				if (QueryBuilder_Dialog == null) {
					QueryBuilder_Dialog = new QueryBuilder_Dialog();
				} else {
					QueryBuilder_Dialog.setVisible(true);
				}
			}
		});
		queryBuilder_bttn.setBorder(UIManager.getBorder("Button.border"));
		queryBuilder_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));

		queryBuilder_bttn.setBounds(66, 218, 296, 27);
		frame.getContentPane().add(queryBuilder_bttn);

		JButton exit_bttn = new JButton("\u0412\u044B\u0445\u043E\u0434");
		exit_bttn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));

		exit_bttn.setBounds(320, 313, 100, 27);
		frame.getContentPane().add(exit_bttn);
	}

	protected boolean isAutorityInDB() {
		boolean result = true;
		if (ConnectingToDB_Dialog.con == null) {
			JOptionPane.showMessageDialog(null, "���������� ����������� � ��!",
					"������", JOptionPane.ERROR_MESSAGE);
			return result = false;
		}
		return result;
	}
}
