package jProject;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import java.awt.Font;
import java.awt.Component;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.SwingConstants;

public class ConnectingToDB_Dialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3227266216992960684L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtLogin;
	private JPasswordField pwdPassword;
	public static String url = "jdbc:oracle:thin:@localhost:1522:test";

	public static String host		= "localhost";
	public static String port		= "1522";
	public static String login		= "test";
	public static String password	= "q35XVjsJ2b0D";
	public static String database	= "test";
	
	public static ConnectDB con = null;
	
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		try {
//			ConnectingToDB_Dialog dialog = new ConnectingToDB_Dialog();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public ConnectingToDB_Dialog() {
		setName("ConnecToDB_Frame");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			txtLogin = new JTextField();
			txtLogin.setFont(new Font("Tahoma", Font.PLAIN, 15));
			txtLogin.setText("test");
			txtLogin.setColumns(10);
		}
		{
			pwdPassword = new JPasswordField();
			pwdPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
			pwdPassword.setAlignmentY(Component.BOTTOM_ALIGNMENT);
			pwdPassword.setAlignmentX(Component.RIGHT_ALIGNMENT);
			pwdPassword.setText("q35XVjsJ2b0D");
		}
		JButton button = new JButton("");
		button.addActionListener(new ActionListener() {
			JDialog SettingsDialog;
			public void actionPerformed(ActionEvent e) {
				if (SettingsDialog == null){
					SettingsDialog = new SettingsDB_Dialog();
				} else {
					SettingsDialog.setVisible(true);
				}
			}
		});
		button.setBorderPainted(false);
		button.setBorder(UIManager.getBorder("Button.border"));
		button.setIcon(new ImageIcon(ConnectingToDB_Dialog.class.getResource("/jProject/resourses/settings.png")));
		JLabel label = new JLabel("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0438\u043C\u044F \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044F:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		JLabel label_1 = new JLabel("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043F\u0430\u0440\u043E\u043B\u044C:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap(106, Short.MAX_VALUE)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label)
							.addGap(65)
							.addComponent(button, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
						.addComponent(label_1)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING, false)
							.addComponent(pwdPassword, Alignment.LEADING)
							.addComponent(txtLogin, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(button, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(154, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
					.addContainerGap(62, Short.MAX_VALUE)
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(label_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pwdPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(38))
		);
		gl_contentPanel.linkSize(SwingConstants.HORIZONTAL, new Component[] {txtLogin, pwdPassword, label, label_1});
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
				okButton.addActionListener(new ActionListener() {
					@SuppressWarnings("deprecation")
					public void actionPerformed(ActionEvent arg0) {
						
						login = txtLogin.getText();
						password = pwdPassword.getText();
						
						// ������ �����������
						// host, port � database ��������������� � SettingsDB_Dialog
						con = new ConnectDB(host, port, login,
								password, database);
						System.out.println("URL: jdbc:oracle:thin:@"+host+":"+port+":"+database+"; Login: "+login+"; Password: "+password);
						setVisible(false); 
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
