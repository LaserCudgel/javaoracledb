package jProject;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

public class DefineCompoundKey extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2306319650341065908L;
	private final JPanel contentPanel = new JPanel();
	static Statement st = ConnectingToDB_Dialog.con.stmt;
	JList<String> listOfColumns = new JList<String>();
	
	
	/**
	 * Create the dialog.
	 */
	public DefineCompoundKey(String tableName) {
		final String tableName_copy = tableName.toUpperCase();
		setTitle("\u041A\u043E\u043D\u0441\u0442\u0440\u0443\u043A\u0442\u043E\u0440 \u0441\u043E\u0441\u0442\u0430\u0432\u043D\u044B\u0445 \u043A\u043B\u044E\u0447\u0435\u0439");
		setBounds(100, 100, 320, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		/* ListOfDB OPEN */
		String query = "SELECT column_name FROM user_tab_columns WHERE table_name IN ('"
				+ tableName_copy + "')";
		pullingList(listOfColumns, query);

		JLabel label = new JLabel("\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0441\u0442\u043E\u043B\u0431\u0446\u044B \u043A\u043B\u044E\u0447\u0430");
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(listOfColumns, GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label, GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
							.addGap(4))))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(label, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(listOfColumns, GroupLayout.PREFERRED_SIZE, 356, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						getFinalQuery(tableName_copy, getSelectedCells());
						String query = getFinalQuery(tableName_copy, getSelectedCells());
						try {
							st.executeQuery(query);
							System.out.println("Request executed successfully.");
						} catch (SQLException e) {
							System.out.println("Request failed.");
							JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
									"������", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	
	private static void pullingList(JList<String> aJList, String aQuery) {
//		System.out.println("������ ��� ���������� �����: " + aQuery);
		DefaultListModel<String> DLM = new DefaultListModel<String>();
		String entity = null;
		if (aQuery.toLowerCase().contains("select table_name")) {
			entity = "table_name";
		} else if (aQuery.toLowerCase().contains("select column_name")) {
			entity = "column_name";
		} else {
			JOptionPane
					.showMessageDialog(
							null,
							"�������� ������ � ������������ ����� ������� DefineCompoundKey.pullingList()",
							"������", JOptionPane.ERROR_MESSAGE);
			return;
		}
		ResultSet rs = null;
		try {
			rs = st.executeQuery(aQuery);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String name = rs.getString(entity);
				DLM.addElement(name);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, e1.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		aJList.setModel(DLM);
	}
	
	protected String getSelectedCells() {
		String selectedValues_str = "";
		List<String> selectedValues = listOfColumns.getSelectedValuesList();
		for (Iterator<String> i = selectedValues.iterator(); i
				.hasNext();) {
			selectedValues_str += i.next().toString();
			if (i.hasNext()) {
				selectedValues_str += "','";
			}
		}
		selectedValues_str = "'" + selectedValues_str;
		selectedValues_str.replaceAll("[|]", "','");
		selectedValues_str.replaceAll("\'+", "'");
//		System.out.println(selectedValues_str);
		
		return selectedValues_str+"'";
	}

	protected String getFinalQuery(String aTable, String aColumns) {
		aTable = aTable.replaceAll("'", "");
		aColumns = aColumns.replaceAll("'", "");
		// ALTER TABLE tab ADD PRIMARY KEY (field1,..,fieldN)
		String query = "ALTER TABLE "+aTable+" ADD PRIMARY KEY "+ "(" + aColumns + ")";
		System.out.println(query);
		return query;
	}
}
