package jProject;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.Font;

import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Component;
import java.awt.FlowLayout;

public class QueryView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2383834092842534513L;
	private JPanel contentPane;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public QueryView(String aQuery) {
		setTitle("\u0412\u0438\u0437\u0443\u0430\u043B\u0438\u0437\u0430\u0442\u043E\u0440 \u0437\u0430\u043F\u0440\u043E\u0441\u043E\u0432");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 624, 498);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
				table = new JTable();
				table.setFont(new Font("Tahoma", Font.PLAIN, 15));
				scrollPane.setViewportView(table);
				table.setRowSelectionAllowed(false);
				table.setEnabled(false);
				
				JPanel panel = new JPanel();
				FlowLayout flowLayout = (FlowLayout) panel.getLayout();
				flowLayout.setAlignment(FlowLayout.RIGHT);
				panel.setAlignmentX(Component.RIGHT_ALIGNMENT);
				GroupLayout gl_contentPane = new GroupLayout(contentPane);
				gl_contentPane.setHorizontalGroup(
					gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 608, GroupLayout.PREFERRED_SIZE))
							.addContainerGap())
				);
				gl_contentPane.setVerticalGroup(
					gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
				);
				
				JButton button = new JButton("\u0417\u0430\u043A\u0440\u044B\u0442\u044C");
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
					}
				});
				panel.add(button);
				button.setFont(new Font("Tahoma", Font.PLAIN, 15));
				contentPane.setLayout(gl_contentPane);

		Statement st = ConnectingToDB_Dialog.con.stmt;
		try {
			st.executeQuery(aQuery);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		rePaintTable(aQuery);
	}
	
	protected void rePaintTable(String aQuery) {
		DatabaseTableModel DTM = new DatabaseTableModel();
		Statement st = ConnectingToDB_Dialog.con.stmt;

		ResultSet rs = null;
		try {
			rs = st.executeQuery(aQuery);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			DTM.setDataSource(rs);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		table.setModel(DTM);
	}
}
