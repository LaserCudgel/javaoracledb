package jProject;

/**
 * This model work with any database table. You should only set an object of java.sql.ResultSet into it.
 * This table cannot insert data into database.
 */

import java.util.*;
import java.sql.*;

import javax.swing.table.*;

public class DatabaseTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private ArrayList<String> columnNames = new ArrayList<String>();
	@SuppressWarnings("rawtypes")
	private ArrayList<Class> columnTypes = new ArrayList<Class>();
	private ArrayList<ArrayList<Object>> data = new ArrayList<ArrayList<Object>>();

    public void addRow()
    {
    	Object obj = new Object[]{null, null, null};
    	ArrayList<Object> arlst = new ArrayList<Object>();
    	arlst.add(obj);
        data.add(arlst);
//        fireTableRowsInserted(data.size(), data.size());
    	fireTableDataChanged();
    }
	
	public int getRowCount() {
		synchronized (data) {
			return data.size();
		}
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public Object getValueAt(int row, int col) {
		synchronized (data) {
			return data.get(row).get(col);
		}
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	public Class<?> getColumnClass(int col) {
		return columnTypes.get(col);
	}

	public boolean isCellEditable(int row, int col) {
		return true;
	}

	public void setValueAt(Object obj, int row, int col) {
		synchronized (data) {
			data.get(row).set(col, obj);
		}
	}

	/**
	 * Core of the model. Initializes column names, types, data from ResultSet.
	 * 
	 * @param rs
	 *            ResultSet from which all information for model is token.
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void setDataSource(ResultSet rs) throws SQLException,
			ClassNotFoundException {
		ResultSetMetaData rsmd = rs.getMetaData();
		columnNames.clear();
		columnTypes.clear();
		data.clear();

		int columnCount = rsmd.getColumnCount();
		for (int i = 0; i < columnCount; i++) {
			columnNames.add(rsmd.getColumnName(i + 1));
			Class<?> type = Class.forName(rsmd.getColumnClassName(i + 1));
			columnTypes.add(type);
		}
		fireTableStructureChanged();
		while (rs.next()) {
			ArrayList<Object> rowData = new ArrayList<Object>();
			for (int i = 0; i < columnCount; i++) {
				if (columnTypes.get(i) == String.class)
					rowData.add(rs.getString(i + 1));
				else
					rowData.add(rs.getObject(i + 1));
			}
			synchronized (data) {
				data.add(rowData);
				this.fireTableRowsInserted(data.size() - 1, data.size() - 1);
			}
		}
	}

/*	// ***TEST***
	public static void main(String[] args) {
		try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			String url = "jdbc:oracle:thin:@localhost:1522:test"; // your data
//			String user = "test"; // your data
//			String password = "q35XVjsJ2b0D"; // your data
			String query = "SELECT * FROM BUS"; // your data
//			Connection con = DriverManager.getConnection(url,
//					user, password);
//			Statement st = con.createStatement();
			Statement st = ConnectingToDB_Dialog.con.stmt;
			ResultSet rs = st.executeQuery(query);
			DatabaseTableModel model = new DatabaseTableModel();
			model.setDataSource(rs);
			JTable table = new JTable(model);

			JPanel panel = new JPanel(new BorderLayout());
			panel.add(new JScrollPane(table), BorderLayout.CENTER);

			JFrame frame = new JFrame("Database Table Model");
			frame.setLocationRelativeTo(null);
			frame.setSize(500, 400);
			frame.setContentPane(panel);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setVisible(true);

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}*/
}