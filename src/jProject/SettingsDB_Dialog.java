package jProject;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SettingsDB_Dialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6096406452967356768L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtName;
	private JTextField txtPort;
	private JTextField txtLocalhost;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			SettingsDB_Dialog dialog = new SettingsDB_Dialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public SettingsDB_Dialog() {
		setName("SettngsDB_Frame");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JLabel label = new JLabel("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0438\u043C\u044F \u0431\u0430\u0437\u044B \u0434\u0430\u043D\u043D\u044B\u0445:");
		
		txtName = new JTextField();
		txtName.setName("txtNameDB");
		txtName.setText("test");
		txtName.setColumns(10);
		
		JLabel label_1 = new JLabel("\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u043F\u043E\u0440\u0442:");
		
		txtPort = new JTextField();
		txtPort.setName("txtPort");
		txtPort.setText("1522");
		txtPort.setColumns(10);
		
		JLabel label_2 = new JLabel("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0430\u0434\u0440\u0435\u0441 \u0445\u043E\u0441\u0442\u0430:");
		
		txtLocalhost = new JTextField();
		txtLocalhost.setName("txtHost");
		txtLocalhost.setText("localhost");
		txtLocalhost.setColumns(10);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap(135, Short.MAX_VALUE)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(label_2)
						.addComponent(label)
						.addComponent(txtName, GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
						.addComponent(label_1)
						.addComponent(txtPort)
						.addComponent(txtLocalhost))
					.addGap(125))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap(24, Short.MAX_VALUE)
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(label_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtPort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(label_2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtLocalhost, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
//						String url = "jdbc:oracle:thin:@localhost:1522:test";
//						String url = "jdbc:oracle:thin:@"+txtLocalhost.getText()+":"+txtPort.getText()+":"+txtName.getText();
						
						ConnectingToDB_Dialog.host = txtLocalhost.getText();
						ConnectingToDB_Dialog.port = txtPort.getText();
						ConnectingToDB_Dialog.database = txtName.getText();
						
//						UI.statusLabel.setText(url);
						setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
