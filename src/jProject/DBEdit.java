package jProject;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import java.awt.Component;

import javax.swing.JButton;

import java.awt.FlowLayout;

import javax.swing.JList;
import javax.swing.ButtonGroup;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import javax.swing.ListSelectionModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DBEdit extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7676404858327121274L;
	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	static Statement st = ConnectingToDB_Dialog.con.stmt;
	public String tableName = null;

	static JList<String> listOfTables = new JList<String>();
	JList<String> listOfColumns = new JList<String>();

	/**
	 * Create the frame.
	 */
	public DBEdit() {
		setTitle("\u041A\u043E\u043D\u0441\u0442\u0440\u0443\u043A\u0442\u043E\u0440 \u0442\u0430\u0431\u043B\u0438\u0446");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 490, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		pullingList(listOfTables, "SELECT table_name FROM user_tables");

		JScrollPane scrollPane = new JScrollPane();

		JLabel label = new JLabel(
				"\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0442\u0430\u0431\u043B\u0438\u0446\u0443");
		label.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panel.setFont(new Font("Tahoma", Font.PLAIN, 15));

		final JButton deleteTable_bttn = new JButton(
				"\u0423\u0434\u0430\u043B\u0438\u0442\u044C");
		deleteTable_bttn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteTable();
			}
		});
		buttonGroup.add(deleteTable_bttn);
		deleteTable_bttn.setEnabled(false);
		deleteTable_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JButton addTable_bttn = new JButton(
				"\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C");
		addTable_bttn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComponent[] inputs = new JComponent[2]; // ������ ������������
															// ��������
				inputs[0] = new JLabel("������� �������� �������");
				inputs[0].setFont(new Font("Tahoma", Font.PLAIN, 15));

				Object[] message = { "������� �������� �������" };
				tableName = JOptionPane.showInputDialog(null, message,
						null, JOptionPane.OK_CANCEL_OPTION);
				if (!tableName.isEmpty()) {
					System.out.println("��������� ����������� ������ ��� �������: " + tableName);
					jProject.TableConstructor TableConstructor = new TableConstructor(tableName);
					TableConstructor.setVisible(true);
				} else {
					System.out.println("������: �������� ������� �� �������!");
					JOptionPane.showMessageDialog(null,
							"�������� ������� �� �������", "������",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		buttonGroup.add(addTable_bttn);
		addTable_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));
		final JButton addColumn_bttn = new JButton(
				"\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C");
		JButton btnNewButton_1 = new JButton("Cancel");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel.add(btnNewButton_1);

		listOfTables.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String query = "SELECT column_name FROM user_tab_columns WHERE table_name IN ("
						+ getSelectedTables() + ")";
				pullingList(listOfColumns, query);

				if (listOfTables.isSelectionEmpty()) {
					deleteTable_bttn.setEnabled(false);
					addColumn_bttn.setEnabled(false);
				} else {
					deleteTable_bttn.setEnabled(true);
					addColumn_bttn.setEnabled(true);
				}
			}
		});
		listOfTables.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(listOfTables);

		JScrollPane scrollPane_1 = new JScrollPane();

		addColumn_bttn.setEnabled(false);
		addColumn_bttn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("��������� ����������� ������ ��� �������: " + listOfTables.getSelectedValue());
				jProject.TableConstructor TableConstructor = new TableConstructor(listOfTables.getSelectedValue());
				TableConstructor.setVisible(true);
			}
		});
		buttonGroup.add(addColumn_bttn);
		addColumn_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));

		final JButton deleteColumn_bttn = new JButton(
				"\u0423\u0434\u0430\u043B\u0438\u0442\u044C");
		deleteColumn_bttn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteColumnFromTable();
			}
		});
		deleteColumn_bttn.setEnabled(false);
		buttonGroup.add(deleteColumn_bttn);
		deleteColumn_bttn.setFont(new Font("Tahoma", Font.PLAIN, 15));

		JLabel label_1 = new JLabel(
				"\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0441\u0442\u043E\u043B\u0431\u0435\u0446");
		label_1.setToolTipText("");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane
				.setHorizontalGroup(gl_contentPane
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								gl_contentPane
										.createSequentialGroup()
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addGap(12)
																		.addGroup(
																				gl_contentPane
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								gl_contentPane
																										.createSequentialGroup()
																										.addComponent(
																												label,
																												GroupLayout.PREFERRED_SIZE,
																												210,
																												GroupLayout.PREFERRED_SIZE)
																										.addGap(18)
																										.addComponent(
																												label_1,
																												GroupLayout.PREFERRED_SIZE,
																												209,
																												GroupLayout.PREFERRED_SIZE))
																						.addGroup(
																								gl_contentPane
																										.createSequentialGroup()
																										.addComponent(
																												scrollPane,
																												GroupLayout.PREFERRED_SIZE,
																												210,
																												GroupLayout.PREFERRED_SIZE)
																										.addGap(18)
																										.addComponent(
																												scrollPane_1,
																												GroupLayout.PREFERRED_SIZE,
																												209,
																												GroupLayout.PREFERRED_SIZE))))
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				deleteTable_bttn,
																				GroupLayout.PREFERRED_SIZE,
																				99,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(12)
																		.addComponent(
																				addTable_bttn)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				18,
																				Short.MAX_VALUE)
																		.addComponent(
																				deleteColumn_bttn)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				addColumn_bttn)))
										.addContainerGap())
						.addComponent(panel, Alignment.LEADING,
								GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE));
		gl_contentPane
				.setVerticalGroup(gl_contentPane
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_contentPane
										.createSequentialGroup()
										.addGap(13)
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(label)
														.addComponent(label_1))
										.addGap(7)
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addComponent(
																scrollPane_1)
														.addComponent(
																scrollPane,
																GroupLayout.DEFAULT_SIZE,
																315,
																Short.MAX_VALUE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																deleteTable_bttn)
														.addComponent(addTable_bttn)
														.addGroup(
																gl_contentPane
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				addColumn_bttn)
																		.addComponent(
																				deleteColumn_bttn)))
										.addGap(18)
										.addComponent(panel,
												GroupLayout.DEFAULT_SIZE, 37,
												Short.MAX_VALUE)
										.addContainerGap()));
		gl_contentPane.linkSize(SwingConstants.HORIZONTAL, new Component[] {
				deleteTable_bttn, addTable_bttn, addColumn_bttn, deleteColumn_bttn });

		listOfColumns.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listOfColumns.isSelectionEmpty()) {
					deleteColumn_bttn.setEnabled(false);
				} else {
					deleteColumn_bttn.setEnabled(true);
				}
			}
		});
		listOfColumns.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_1.setViewportView(listOfColumns);
		contentPane.setLayout(gl_contentPane);
	}

	protected void deleteColumnFromTable() {
		String columnName = listOfColumns.getSelectedValue().toString();
		String tableName = listOfTables.getSelectedValue().toString();
		// ALTER TABLE TableName DROP COLUMN ColumnName;
		String query = "ALTER TABLE  " + tableName + " DROP COLUMN "
				+ columnName;
		System.out.println(query);
		int reply = JOptionPane.showConfirmDialog(null,
				"������������� ������� ������� " + tableName + " �� ������� "
						+ columnName + "?", null, JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
			try {
				st.executeQuery(query);
				System.out.println("Request executed successfully.");
			} catch (SQLException e) {
				System.out.println("Request failed.");
				JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
						"������", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
			query = "SELECT column_name FROM user_tab_columns WHERE table_name IN ("
					+ getSelectedTables() + ")";
			pullingList(listOfColumns, query);
		}
	}

	protected void deleteTable() {
		String tableName = listOfTables.getSelectedValue().toString();
		String query = "DROP TABLE " + tableName;
		System.out.println(query);
		int reply = JOptionPane.showConfirmDialog(null,
				"������������� ������� ������� " + tableName + "?", null,
				JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.YES_OPTION) {
			try {
				st.executeQuery(query);
				System.out.println("Request executed successfully.");
			} catch (SQLException e) {
				System.out.println("Request failed.");
				JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
						"������", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
			pullingList(listOfTables, "SELECT table_name FROM user_tables");
		}
	}
	
	public static void rePullingListDB(){
		pullingList(listOfTables, "SELECT table_name FROM user_tables");
	}

	private static void pullingList(JList<String> aJList, String aQuery) {
//		System.out.println("������ ��� ���������� �����: " + aQuery);
		DefaultListModel<String> DLM = new DefaultListModel<String>();
		String entity = null;
		if (aQuery.toLowerCase().contains("select table_name")) {
			entity = "table_name";
		} else if (aQuery.toLowerCase().contains("select column_name")) {
			entity = "column_name";
		} else {
			JOptionPane
					.showMessageDialog(
							null,
							"�������� ������ � ������������ ����� ������� DBEdit.pullingList()",
							"������", JOptionPane.ERROR_MESSAGE);
			return;
		}
		ResultSet rs = null;
		try {
			rs = st.executeQuery(aQuery);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String name = rs.getString(entity);
				DLM.addElement(name);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		try {
			rs.close();
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, e1.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		aJList.setModel(DLM);
	}

	protected String getSelectedTables() {
		String selectedValues_str = "";
		List<String> selectedValues = listOfTables.getSelectedValuesList();
		for (Iterator<String> i = selectedValues.iterator(); i.hasNext();) {
			selectedValues_str += i.next().toString();
			if (i.hasNext()) {
				selectedValues_str += "','";
			}
		}
		selectedValues_str = "'" + selectedValues_str;
		selectedValues_str.replaceAll("[|]", "','");
		selectedValues_str.replaceAll("\'+", "'");
		// System.out.println(query);

		return selectedValues_str + "'";
	}
}
