package jProject;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class ConnectDB {
    public Statement stmt;
    public Connection con;
    
    String host = null;
    String port = null;
    String user = null;
    String password = null;
    String database = null;
    public String url = "";
    
	
	public ConnectDB(String aHost, String aPort, String aUser, String aPassword, String aDatabase) {
        this.setHost(aHost);
        this.setPort(aPort);
        this.setUser(aUser);
        this.setPassword(aPassword);
        this.setDatabase(aDatabase);
        
        init();
	}

	public void init() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			url = getUrl();
			user = getUser();
			password = getPassword();
			System.out.println(url);
			con = DriverManager.getConnection(url, user, password);
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
			        ResultSet.CONCUR_UPDATABLE);
			UI.statusLabel.setText("�� ������� �����, ��� "+user);
			System.out.println("Connected.");
		} catch (ClassNotFoundException | SQLException e) {
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),
					"������", JOptionPane.ERROR_MESSAGE);
			UI.statusLabel.setText("�������� ������ �����������");
			closeCon();
			e.printStackTrace();
		}
	}
	
	PreparedStatement prepareStatement(String sql){
		try {
			return con.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
    public boolean acceptsConnection() {
        try {
            Driver drv =  DriverManager.getDriver(url);
            return drv.acceptsURL(url);
        } catch (SQLException e) {
        	e.printStackTrace();
            return false;
        }   
    }
	
    public void closeCon() {
        try {
			stmt.close();
			con.close();
			UI.statusLabel.setText("�� �� �����");
			System.out.println("Disconnected.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
	
    public boolean isClosed() {
        try {
            return con.isClosed();
        } catch (Exception e) {
            return true;
        }
    }
	
    public Statement getStmt() {
        return stmt;
    }
    
    public void setStmt(Statement  arg0) {
        this.stmt = arg0;
    }
    
    public Connection getCon() {
        return con;
    }
    
    public void setCon(Connection  arg0) {
        this.con = arg0;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String  arg0) {
        this.host = arg0;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String  arg0) {
        this.user = arg0;
    }
    
    public String getPort() {
        return port;
    }

    public void setPort(String  arg0) {
        this.port= arg0;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String  arg0) {
        this.password = arg0;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String  arg0) {
        this.database = arg0;
    }
    
    public String getUrl() {
//    	return "jdbc:oracle:thin:@localhost:1522:test";
    	return "jdbc:oracle:thin:@"+this.getHost()+":"+this.getPort()+":"+this.getDatabase();
    }

	public DatabaseMetaData getMetaData() {
		try {
			return con.getMetaData();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
